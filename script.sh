#!/bin/bash

# stop all containers
docker stop $(docker ps -a -q)

# remove all existed containers
docker rm $(docker ps -a -q)

# remove all images
docker rmi $(docker images -a -q)

# build docker image
docker build -t backend-facedetect-app .

# run docker image and expose server
docker run -p 7050:7050 backend-facedetect-app
